﻿ping(controller)
{	controller.setContext("ping")
	instructions := object()
	instructions.insert("esc/q: quit")
	instructions.insert("c: Disable hotkeys and clear the screen. Use Alt + c to re-enable the hotkeys")
	instructions.insert("g: ping google")
	instructions.insert("i: Ipconfig")
	instructions.insert("p: Ipconfig /all")
	instructions.insert("a: Adapter settings")
	instructions.insert("g: ping www.google.com")
	instructions.insert("r: Ipconfig /release/renew")
	instructions.insert("d: Open command prompt")
	instructions.insert("m: Open IPChanger")
	controller.showMessage(arrayToString(instructions), ignoreClicks := true, ignoreEsc := true)
	return
}

#if globalController.getContext() == "ping suspended"
!c::
{	globalController.setContext("ping")
	ping(globalController)
	KeyWait, alt
	KeyWait, c
	return
}

#if globalController.getContext() == "ping"
esc::
q::
{	globalController.clearDisplay()
	globalController.setContext("")
	return
}
g::
{	Run, %comspec% /c ping www.google.com.au && Pause
	return
}
i::
{	run, %comspec% /c ipconfig && pause, , Max
	return
}
p::
{	run, %comspec% /c ipconfig /all && pause, , Max
	return
}
a::
{	run, ncpa.cpl
	return
}
r::
{	run, %comspec% /c ipconfig /release && ipconfig /renew && pause, , Max
	return
}
c::
{ 	globalController.clearDisplay()
	globalController.setContext("ping suspended")
	return
}
d::
{	run, cmd.exe
	return
}

m::
{	gosub, IPChanger
	return
}
